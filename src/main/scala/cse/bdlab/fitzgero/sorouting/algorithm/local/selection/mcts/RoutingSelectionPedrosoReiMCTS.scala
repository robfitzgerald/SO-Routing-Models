package cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts

import scala.collection.{GenIterable, GenMap, GenSeq}

import cse.bdlab.fitzgero.mcts.core._
import cse.bdlab.fitzgero.mcts.variant._
import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.Tag._
import cse.bdlab.fitzgero.sorouting.model.path.SORoutingPathSegment
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.local.{LocalGraph, LocalODPair}

// WARNING! changes to this file need to be updated in the separate "SO-Routing-Models" package and resulting .jar file stored in lib/
trait RoutingSelectionPedrosoReiMCTS extends PedrosoReiMCTS[AlternatesSet, Tag] with Serializable {

  def graph: LocalGraph
  def request: GenMap[LocalODPair, GenSeq[List[SORoutingPathSegment]]]
  def seed: Long
  //  def timeBudget: Long

  //  val objective: Objective = Minimize
  val globalAlternates: Tag.GlobalAlternates = Tag.repackage(request)

  /**
    * helper that transforms tags back into OD requests and corresponding paths
    * @param solution a sequence of unique alternate path tags
    * @return
    */
  def unTag(solution: AlternatesSet): GenMap[LocalODPair, List[SORoutingPathSegment]] = Tag.unTagAlternates(solution, globalAlternates)

  /**
    * generates possible actions in such a way that there are no symmetrical subtrees
    * @param state the given state
    * @return a sequence of actions
    */
  override def generatePossibleActions(state: AlternatesSet): Seq[Tag] = {
    val alts: GenIterable[(Int, (LocalODPair, List[SORoutingPathSegment]))] = Tag.nextUnassignedPersonAlts(state, globalAlternates).getOrElse(Seq())
    val result = alts.map { alt => Tag.createTagFrom(alt._2._1, alt._1) }
    if (result.nonEmpty) result.toList else List()
  }

  override def stateIsNonTerminal(state: AlternatesSet): Boolean = state.size < globalAlternates.size
  override def applyAction(state: AlternatesSet, action: Tag): AlternatesSet = state :+ action
  override def selectAction(actions: Seq[Tag]): Option[Tag] = actionSelection.selectAction(actions)

  // utilities
  //  override def startState: AlternatesSet = Seq()
  //  override def samplingMethod: MCTSAlgorithm[Tree, UCTScalarStandardReward.Coefficients] = UCTScalarStandardReward()
  override def actionSelection: ActionSelection[AlternatesSet, Tag] = RandomSelection(random, generatePossibleActions)
  override val random: RandomGenerator = new BuiltInRandomGenerator(Some(seed))
  //  override val terminationCriterion: TimeTermination[AlternatesSet,Tag,Tree] = TimeTermination[AlternatesSet,Tag,Tree](timeBudget)
}