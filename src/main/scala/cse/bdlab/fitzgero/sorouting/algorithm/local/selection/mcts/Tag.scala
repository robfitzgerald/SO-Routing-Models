package cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts

import scala.annotation.tailrec
import scala.collection.{GenMap, GenSeq}

import cse.bdlab.fitzgero.sorouting.model.path.SORoutingPathSegment
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.local.LocalODPair

// WARNING! changes to this file need to be updated in the separate "SO-Routing-Models" package and resulting .jar file stored in lib/

/**
  * MCTS 'Action' Type [A] for tree search, an alternate path represented by a label
  * @param personId the name of the person associated with some set of alternates
  * @param alternate the position of this alternate in the Seq[Path] that has the edges used
  */
case class Tag(personId: Tag.PersonID, alternate: Int) extends Serializable {
  override def toString: String = s"$personId${Tag.toStringSeparator}$alternate"
}

object Tag {
  def toStringSeparator: String = """-----"""
  type PersonID = String
  type PersonAlternatePaths = GenMap[Int, (LocalODPair, List[SORoutingPathSegment])]
  type GlobalAlternates = GenMap[PersonID, PersonAlternatePaths]


  /**
    * MCTS 'State' Type [S] for tree search, a collection of alternate paths
    */
  type AlternatesSet = Seq[Tag]

  def createTagFrom(localODPair: LocalODPair, index: Int): Tag = Tag(localODPair.id, index)

  /**
    * creates a global set of alternate path data which can be inspected by Tags to find relevant data
    * @param request
    * @return
    */
  def repackage(request: GenMap[LocalODPair, GenSeq[List[SORoutingPathSegment]]]): GlobalAlternates = {
    request
      .map {
        outerTuple =>
          (outerTuple._1.id: PersonID) -> outerTuple._2.zipWithIndex.map {
            innerTuple =>
              innerTuple._2 -> (outerTuple._1, innerTuple._1)
          }.toMap
      }
  }

  def grabOriginalsAssociatedWith(tag: Tag, alts: GlobalAlternates): Option[(LocalODPair, List[SORoutingPathSegment])] = {
    for {
      thisPersonsAlts <- alts.get(tag.personId)
      thisAlt <- thisPersonsAlts.get(tag.alternate)
    } yield {
      thisAlt
    }
  }

  /**
    * given some state, we return the alternate paths of at most one additional driver
    * NOTE: this guarantees subtrees will not be symmetric only when alts is invariant between calls
    * @param state the alternate paths already selected in this possible partial solution of our search
    * @param alts the global list of our options
    * @return the set of alternates associated with the person we want to add to this state
    */
  def nextUnassignedPersonAlts(state: AlternatesSet, alts: GlobalAlternates): Option[PersonAlternatePaths] = {
    @tailrec
    def _search(s: AlternatesSet, a: GlobalAlternates): Option[PersonAlternatePaths] = {
      if (s.isEmpty) a.headOption.map{_._2}
      else _search(s.tail, a - s.head.personId)
    }
    _search(state,alts)
  }

  def unTagAlternates(solution: AlternatesSet, globalAlts: GlobalAlternates): GenMap[LocalODPair, List[SORoutingPathSegment]] = {
    (for {
      tag <- solution
      alt <- grabOriginalsAssociatedWith(tag, globalAlts)
    } yield {
      alt
    }).toMap
  }
}
