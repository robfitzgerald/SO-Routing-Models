package cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts

import scala.collection.{GenMap, GenSeq}

import cse.bdlab.fitzgero.mcts.algorithm.samplingpolicy.banditfunction.UCT_PedrosoRei.{Minimize, Objective}
import cse.bdlab.fitzgero.mcts.algorithm.samplingpolicy.scalar.UCTScalarPedrosoReiReward.{Coefficients, ExplorationCoefficient, SearchCoefficient}
import cse.bdlab.fitzgero.mcts.core.terminationcriterion.{TerminationCriterion, TimeTermination}
import cse.bdlab.fitzgero.mcts.tree.MCTreePedrosoReiReward
import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.Tag._
import cse.bdlab.fitzgero.sorouting.model.path.SORoutingPathSegment
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.local.{LocalGraph, LocalODPair}

class RoutingSelectionPedrosoReiMCTSSolver (
  val graph              : LocalGraph,
  val request            : GenMap[LocalODPair, GenSeq[List[SORoutingPathSegment]]],
  val seed               : Long = 0L,
  override val terminationCriterion: TerminationCriterion[Tag.AlternatesSet, Tag, MCTreePedrosoReiReward[Tag.AlternatesSet, Tag]] = TimeTermination(5000L),
  val startState         : AlternatesSet = Seq() ) extends RoutingSelectionPedrosoReiMCTS {

  override val objective: Objective = Minimize()
  override var globalBestSimulation: BigDecimal = objective.defaultBest
  override var globalWorstSimulation: BigDecimal = objective.defaultWorst
  override var bestSolution: AlternatesSet = Seq()

  override def getSearchCoefficients(tree: Tree): Coefficients =
    Coefficients(ExplorationCoefficient, globalBestSimulation, globalWorstSimulation)

  override def getDecisionCoefficients(tree: Tree): Coefficients =
    Coefficients(SearchCoefficient, globalBestSimulation, globalWorstSimulation)

  override def evaluateTerminal(state: AlternatesSet): BigDecimal =
    MCTSUtilities.evaluateCongestionSumForChangedAsBigDecimal(state, globalAlternates, graph)

}

object RoutingSelectionPedrosoReiMCTSSolver {
  def apply(graph: LocalGraph, request: GenMap[LocalODPair, GenSeq[List[SORoutingPathSegment]]], seed: Long, terminationCriterion: TerminationCriterion[Tag.AlternatesSet, Tag, MCTreePedrosoReiReward[Tag.AlternatesSet, Tag]]): RoutingSelectionPedrosoReiMCTSSolver =
    new RoutingSelectionPedrosoReiMCTSSolver(graph, request, seed, terminationCriterion)
  def apply(graph: LocalGraph, request: GenMap[LocalODPair, GenSeq[List[SORoutingPathSegment]]], seed: Long, terminationCriterion: TerminationCriterion[Tag.AlternatesSet, Tag, MCTreePedrosoReiReward[Tag.AlternatesSet, Tag]], startState: AlternatesSet): RoutingSelectionPedrosoReiMCTSSolver =
    new RoutingSelectionPedrosoReiMCTSSolver(graph, request, seed, terminationCriterion, startState)
  def apply(graph: LocalGraph, request: GenMap[LocalODPair, GenSeq[List[SORoutingPathSegment]]]): RoutingSelectionPedrosoReiMCTSSolver =
    new RoutingSelectionPedrosoReiMCTSSolver(graph, request)
}