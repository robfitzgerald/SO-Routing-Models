package cse.bdlab.fitzgero.sorouting.algorithm.local.selection

import scala.collection.{GenMap, GenSeq}
import scala.concurrent.Await
import scala.concurrent.duration._

import cse.bdlab.fitzgero.graph.algorithm.GraphAlgorithm
import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.Tag
import cse.bdlab.fitzgero.sorouting.model.path.SORoutingPathSegment
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.local.{LocalGraph, LocalODPair}
import cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.HackSerializer
import cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.monolithic.MonolithicTaskParallelLivyJob
import org.apache.livy.scalaapi.{LivyScalaClient, ScalaJobHandle}

// WARNING! changes to this file need to be updated in the separate "SO-Routing-Models" package and resulting .jar file stored in lib/

object PedrosoReiSparkMonolithicAlgorithm extends GraphAlgorithm {

  /////// reference types
  override type VertexId = String //KSPLocalDijkstrasAlgorithm.VertexId
  override type EdgeId = String //KSPLocalDijkstrasAlgorithm.EdgeId
  override type Graph = LocalGraph //KSPLocalDijkstrasAlgorithm.Graph
  type Path = List[SORoutingPathSegment]

  /////// algorithm API
  override type AlgorithmRequest = GenMap[LocalODPair, GenSeq[Path]]
  override type AlgorithmConfig = {
    def matsimDataRepositoryDirectory: String
    def randomSeed: Long
    def computationalLimit: Long // ms. spent searching before decision
    def numSparkExecutors: Int
    def maxTasksPerWorker: Int
    def livyClient: LivyScalaClient
    def timeWindow: Int
    def sourceAssetsDirectory: String
  }
  case class AlgorithmResult(result: GenMap[LocalODPair, Path], logs: Map[String,String])


  override def runAlgorithm(graph: LocalGraph, request: AlgorithmRequest, config: Option[AlgorithmConfig]): Option[AlgorithmResult] = {
    if (SORoutingPathSegment.hasNoOverlap(request.values.toSeq)) {
      println("[mCTS02] request was found to have no overlapping edges. reverting to greedy solution.")
      None
    } else {
      // for now, since the anonymous function wrapper of flatMap-ing on config is producing
      // a class not found error...
      val livyClient = config.get.livyClient
      val sourceAssetDirectory = config.get.sourceAssetsDirectory
      val matsimDataRepositoryDirectory = config.get.matsimDataRepositoryDirectory
      val randomSeed = config.get.randomSeed
      val computationalLimit = config.get.computationalLimit
      val numSparkExecutors = config.get.numSparkExecutors
      val maxTasksPerWorker = config.get.maxTasksPerWorker
      val timeWindow = config.get.timeWindow

      val job: MonolithicTaskParallelLivyJob = new MonolithicTaskParallelLivyJob(
        livyClient,
        matsimDataRepositoryDirectory,
        sourceAssetDirectory,
        graph,
        request,
        randomSeed,
        computationalLimit,
        timeWindow,
        numSparkExecutors,
        maxTasksPerWorker
      )

      val submitted: ScalaJobHandle[String] = job.run()
      val result = Await.result(submitted, (computationalLimit * numSparkExecutors * maxTasksPerWorker) milliseconds)
      println(result)
      val repackagedResult = HackSerializer.stringDecodeResult(result)

      if (repackagedResult.hasSolution) {
        Some(AlgorithmResult(Tag.unTagAlternates(repackagedResult.bestSimulation, Tag.repackage(request)), repackagedResult.logs))
      } else {
        None
      }
    }
  }
}
