package cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts

import scala.collection.GenMap
import scala.util.Random

import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.Tag.{AlternatesSet, GlobalAlternates}
import cse.bdlab.fitzgero.sorouting.model.path.SORoutingPathSegment
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.local.{LocalGraph, LocalGraphOps}

/**
  * Provides pseudorandom support functions for MCTS
  * @param randomSeed an optional random seed value, used for testing
  * @param CongestionRatioThreshold the percent that congestion is allowed to grow within a state where it is still given a reward
  * @param globalAlts the list of all possible alternate paths for each person
  */
class MCTSUtilities(randomSeed: Option[Int], CongestionRatioThreshold: Double, globalAlts: GenMap[Tag.PersonID, GenMap[Tag, Seq[String]]]) {

  val random: Random = randomSeed match {
    case Some(seed) => new Random(seed)
    case None => new Random
  }

  /**
    * gives a reward value only if none of the costs increase by CongestionRatioThreshold
    * fails to
    * @param costs the edges paired with their starting costs and the costs from this group
    * @return 1 or 0
    */
  def forAllCostDiff(costs: List[(String, Double, Double)]): Int = {
    val testResult = costs.forall {
      cost =>
        if (cost._2 == 0) true // @TODO: this would fail us when a flow transitions from 0 to something above the congestion threshold!
        else (cost._3 / cost._2) <= CongestionRatioThreshold
    }
    if (testResult) 1 else 0
  }

  /**
    * gives a reward value if the average of the costs do not exceed CongestionRatioThreshold
    * @param costs the edges paired with their starting costs and the costs from this group
    * @return 1 or 0
    */
  def meanCostDiff(costs: List[(String, Double, Double)]): Int = {
    if (costs.isEmpty) {
      1 // TODO: costs should never be empty since this algorithm returns None on an empty request.
    } else {
      val avgCostDiff: Double = costs.map {
        tuple =>
          if (tuple._2 == 0) 0.0D
          else tuple._3 / tuple._2
      }.sum / costs.size
      val testResult: Boolean = avgCostDiff <= CongestionRatioThreshold
      if (testResult) 1 else 0
    }
  }
}

object MCTSUtilities {
  def apply(random: Option[Int], CongestionRatioThreshold: Double, globalAlts: GenMap[Tag.PersonID, GenMap[Tag, Seq[String]]]): MCTSUtilities =
    new MCTSUtilities(random, CongestionRatioThreshold, globalAlts)

  def evaluateCongestionSumForChangedAsBigDecimal(theseAlts: AlternatesSet, globalAlts: GlobalAlternates, graph: LocalGraph): BigDecimal = {
    val edgesAndFlows = edgesAndFlowsIn(allPathSegmentsIn(theseAlts, globalAlts))
    val updatedGraph = LocalGraphOps.updateGraph(edgesAndFlows, graph)
    val edgeUsedInSet = edgesAndFlows.keySet
    val edgesToEvaluate = updatedGraph.edges.filter{ e => edgeUsedInSet(e._1) }
    (for {
      e <- edgesToEvaluate
      priorEdge <- graph.edgeById(e._1)
      priorCost <- priorEdge.attribute.linkCostFlow
      updatedCost <- e._2.attribute.linkCostFlow
    } yield {
      BigDecimal.decimal(updatedCost - priorCost)
    }).sum
  }

  def allPathSegmentsIn(theseAlts: AlternatesSet, globalAlts: GlobalAlternates): Seq[SORoutingPathSegment] = {
    for {
      alt <- theseAlts
      originals <- Tag.grabOriginalsAssociatedWith(alt, globalAlts).toList
      segment <- originals._2
    } yield segment
  }

  def edgesAndFlowsIn(allSegments: Seq[SORoutingPathSegment]): Map[SORoutingPathSegment.EdgeId, Int] =
    allSegments
      .groupBy { _.edgeId }
      .mapValues { _.size }

}