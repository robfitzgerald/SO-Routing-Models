package cse.bdlab.fitzgero.sorouting.algorithm.local.selection

import scala.collection.{GenMap, GenSeq}
import scala.concurrent.Await
import scala.concurrent.duration._

import cse.bdlab.fitzgero.graph.algorithm.GraphAlgorithm
import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.Tag
import cse.bdlab.fitzgero.sorouting.model.path.SORoutingPathSegment
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.local.{LocalGraph, LocalODPair}
import cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.HackSerializer
import cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.iterative.{IterativeLivyJob, IterativeLivyJobConfig}
import org.apache.livy.scalaapi.{LivyScalaClient, ScalaJobHandle}

object PedrosoReiSparkIterativeAlgorithm extends GraphAlgorithm {

  /////// reference types
  override type VertexId = String
  override type EdgeId = String
  override type Graph = LocalGraph
  type Path = List[SORoutingPathSegment]

  /////// algorithm API
  override type AlgorithmRequest = GenMap[LocalODPair, GenSeq[Path]]
  override type AlgorithmConfig = {
    def matsimDataRepositoryDirectory: String
    def randomSeed: Long
    def computationalLimit: Long // ms. spent searching before decision
    def livyClient: LivyScalaClient
    def timeWindow: Int
    def sourceAssetsDirectory: String
    def iterativeLivyJobConfig: IterativeLivyJobConfig
  }
  case class AlgorithmResult(result: GenMap[LocalODPair, Path], logs: Map[String,String])


  override def runAlgorithm(graph: LocalGraph, request: AlgorithmRequest, config: Option[AlgorithmConfig]): Option[AlgorithmResult] = {
    if (SORoutingPathSegment.hasNoOverlap(request.values.toSeq)) {
      println("[mCTS02] request was found to have no overlapping edges. reverting to greedy solution.")
      None
    } else {
      // for now, since the anonymous function wrapper of flatMap-ing on config is producing
      // a class not found error...
      val livyClient = config.get.livyClient
      val sourceAssetDirectory = config.get.sourceAssetsDirectory
      val matsimDataRepositoryDirectory = config.get.matsimDataRepositoryDirectory
      val randomSeed = config.get.randomSeed
      val computationalLimit = config.get.computationalLimit
      val timeWindow = config.get.timeWindow
      val iterativeLivyJobConfig = config.get.iterativeLivyJobConfig


      val job: IterativeLivyJob = new IterativeLivyJob(
        livyClient,
        matsimDataRepositoryDirectory,
        sourceAssetDirectory,
        graph,
        request,
        randomSeed,
        computationalLimit,
        timeWindow,
        iterativeLivyJobConfig
      )

      val submitted: ScalaJobHandle[String] = job.run()
      val result = Await.result(submitted, (computationalLimit * 10) milliseconds)
      println(result)
      val repackagedResult = HackSerializer.stringDecodeResult(result)

      if (repackagedResult.hasSolution) {
        Some(AlgorithmResult(Tag.unTagAlternates(repackagedResult.bestSimulation, Tag.repackage(request)), repackagedResult.logs))
      } else {
        None
      }
    }
  }
}
