package cse.bdlab.fitzgero.sorouting.model.roadnetwork.local

import cse.bdlab.fitzgero.graph.propertygraph.PropertyEdge
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.costfunction._

// WARNING! changes to this file need to be updated in the separate "SO-Routing-Models" package and resulting .jar file stored in lib/

case class LocalEdge (
  id: String,
  src: String,
  dst: String,
  attribute: LocalEdgeAttribute with CostFunction
) extends PropertyEdge with Serializable {
  override type VertexId = String
  override type EdgeId = String
  override type Attr = LocalEdgeAttribute with CostFunction
}

object LocalEdge {
  // Important to note. We will want to expose some of these static variables in the future, as configuration values.
  // For instance, we may be dealing in meters vs feet, and we want realistic congestion values.


  /**
    * data is stored in units per hour.
    */
  val DataFlowRate: Double = 3600D // where 1 is 1 second, 1 hour = 3600 seconds.

  val MetersPerKilometer: Double = 1000D

  def ofDrivers(
    id: String,
    src: String,
    dst: String,
    capacity: Option[Double] = None,
    freeFlowSpeed: Option[Double] = None,
    distance: Option[Double] = None,
    costFunction: Option[CostFunctionType] = None,
    algorithmFlowRate: Double = DataFlowRate): LocalEdge = {

    val capacityScaled: Option[Double] = capacity.map(_ * (algorithmFlowRate / DataFlowRate))
    val freeFlowSpeedScaled: Option[Double] = freeFlowSpeed.map{_ * MetersPerKilometer}

    costFunction match {
      case None =>
        LocalEdge(id, src, dst, new LocalEdgeSimulationAttribute(capacity = capacityScaled, freeFlowSpeed = freeFlowSpeedScaled, distance = distance) with BasicCostFunction)
      case Some(cost) => cost match {
        case BasicCostFunctionType =>
          LocalEdge(id, src, dst, new LocalEdgeSimulationAttribute(capacity = capacityScaled, freeFlowSpeed = freeFlowSpeedScaled, distance = distance) with BasicCostFunction)
        case BPRCostFunctionType =>
          LocalEdge(id, src, dst, new LocalEdgeSimulationAttribute(capacity = capacityScaled, freeFlowSpeed = freeFlowSpeedScaled, distance = distance) with BPRCostFunction)
      }
    }
  }

  def apply(
    id: String,
    src: String,
    dst: String,
    flow: Option[Double] = None,
    capacity: Option[Double] = None,
    freeFlowSpeed: Option[Double] = None,
    distance: Option[Double] = None,
    costFunction: Option[CostFunctionType] = None,
    algorithmFlowRate: Double = DataFlowRate): LocalEdge = {

    val capacityScaled: Option[Double] = capacity.map(_ * (algorithmFlowRate / DataFlowRate))
    val freeFlowSpeedScaled: Option[Double] = freeFlowSpeed.map{_ * MetersPerKilometer}

    costFunction match {
      case None =>
        LocalEdge(id, src, dst, new LocalEdgeFlowAttribute(flow, capacityScaled, freeFlowSpeedScaled, distance) with BasicCostFunction)
      case Some(cost) => cost match {
        case BasicCostFunctionType =>
          LocalEdge(id, src, dst, new LocalEdgeFlowAttribute(flow, capacityScaled, freeFlowSpeedScaled, distance) with BasicCostFunction)
        case BPRCostFunctionType =>
          LocalEdge(id, src, dst, new LocalEdgeFlowAttribute(flow, capacityScaled, freeFlowSpeedScaled, distance) with BPRCostFunction)
      }
    }
  }

  /**
    * helper function to allow flow updates without losing mixin information, that will treat unset flow values as 0.0D
    * @param edge the edge to update
    * @param additionalFlow the value to update with
    * @return
    */
  def modifyFlow(edge: LocalEdge, additionalFlow: Double): LocalEdge = {
    val currentFlow: Double = edge.attribute.flow match {
      case Some(flowValue) => flowValue
      case None => 0D
    }
    edge.attribute match {
      case a: LocalEdgeFlowAttribute with BasicCostFunction =>
        edge.copy(attribute = new LocalEdgeFlowAttribute(Some(currentFlow + additionalFlow), a.capacity, a.freeFlowSpeed, a.distance) with BasicCostFunction)
      case a: LocalEdgeFlowAttribute with BPRCostFunction =>
        edge.copy(attribute = new LocalEdgeFlowAttribute(Some(currentFlow + additionalFlow), a.capacity, a.freeFlowSpeed, a.distance) with BPRCostFunction)
    }
  }

  /**
    * helper function to allow flow updates without losing mixin information, that will treat unset flow values as 0.0D
    * @param edge the edge to update
    * @param newFlowValue the value to update with
    * @return
    */
  def setFlow(edge: LocalEdge, newFlowValue: Double): LocalEdge = {
    edge.attribute match {
      case a: LocalEdgeFlowAttribute with BasicCostFunction =>
        edge.copy(attribute = new LocalEdgeFlowAttribute(Some(newFlowValue), a.capacity, a.freeFlowSpeed, a.distance) with BasicCostFunction)
      case a: LocalEdgeFlowAttribute with BPRCostFunction =>
        edge.copy(attribute = new LocalEdgeFlowAttribute(Some(newFlowValue), a.capacity, a.freeFlowSpeed, a.distance) with BPRCostFunction)
    }
  }
}