package cse.bdlab.fitzgero.sorouting.model.population

import cse.bdlab.fitzgero.graph.population.BasicResponse
import cse.bdlab.fitzgero.sorouting.model.path.SORoutingPathSegment

/**
  * the model-agnostic routing request response
  */
trait SORoutingResponse extends BasicResponse with Serializable {
  type Request <: SORoutingRequest
  override type Path = Seq[SORoutingPathSegment]
}
