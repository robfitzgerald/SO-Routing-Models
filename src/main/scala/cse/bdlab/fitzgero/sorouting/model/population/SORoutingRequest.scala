package cse.bdlab.fitzgero.sorouting.model.population

import java.time.LocalTime

import cse.bdlab.fitzgero.graph.population.BasicRequest

/**
  * a model-agnostic request representation
  */
trait SORoutingRequest extends BasicRequest with Serializable {
  override type TimeUnit = LocalTime
  override type Id = String
}
