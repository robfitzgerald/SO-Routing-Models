package cse.bdlab.fitzgero.sorouting.model.roadnetwork.local

trait LocalEdgeAttribute extends Serializable {
  def flow: Option[Double]
  def capacity: Option[Double]
  def freeFlowSpeed: Option[Double]
  def distance: Option[Double]
}
