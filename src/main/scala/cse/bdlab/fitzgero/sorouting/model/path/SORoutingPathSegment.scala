package cse.bdlab.fitzgero.sorouting.model.path

import scala.annotation.tailrec
import scala.collection.GenSeq

import cse.bdlab.fitzgero.graph.basicgraph.BasicPathSegment

/**
  * Representing a road segment and it's state as a vector of user-defined costs
  * @param edgeId the associated edge by ID as it corresponds to the underlying simulation or real data
  * @param cost an optional vector of calculated costs
  */
case class SORoutingPathSegment (edgeId: String, cost: Option[Seq[Double]]) extends BasicPathSegment with Serializable {
  override type EdgeId = SORoutingPathSegment.EdgeId
}

object SORoutingPathSegment {
  type EdgeId = String
  def hasNoOverlap(request: GenSeq[GenSeq[GenSeq[SORoutingPathSegment]]]): Boolean = {
    @tailrec
    def _hasNoOverlap(req: GenSeq[EdgeId], lookup: Set[EdgeId] = Set()): Boolean = {
      if (req.isEmpty) true
      else if (lookup(req.head)) false
      else _hasNoOverlap(req.tail, lookup + req.head)
    }
    _hasNoOverlap(
      for {
        person <- request
        alt <- person
        edge <- alt
      } yield edge.edgeId
    )
  }
}
