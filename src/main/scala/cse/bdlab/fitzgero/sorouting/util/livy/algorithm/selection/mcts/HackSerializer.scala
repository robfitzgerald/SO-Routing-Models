package cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts

import scala.collection.{GenMap, GenSeq}
import scala.util.matching.Regex

import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.PedrosoReiSparkMonolithicAlgorithm.Path
import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.Tag
import cse.bdlab.fitzgero.sorouting.model.path.SORoutingPathSegment
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.local.{LocalGraph, LocalODPair}

object HackSerializer {
  object Relations {
    /////// HIGH-LEVEL SEPARATORS
    // entries in a transactional object
    val entry: String = """~~~"""
    // spacer between tags
    val tag: String = """###"""
    // spacer between log entries
    val log: String = """%%%"""
    // Map.toString key-value relation
    val kv: String = " -> "


    val none: String = """!NONE!"""
    val request: String = """%REQUEST%"""
    val person: String = """%PERSON%"""
    val alts: String = """%ALTS%"""
    val alt: String = """%ALT%"""
    val src: String = """%SRC%"""
    val dst: String = """%DST%"""
    val path: String = """%PATH%"""
    val segment: String = """%SEGMENT%"""
    val altPaths: String = """%ALTPATHS%"""
    val costs: String = """%COSTS%"""
    val cost: String = """%COST%"""

    val flowSnapshot: String = """%SNAPSHOT%"""
    val edge: String = """%EDGE%"""
    val flow: String = """%FLOW%"""
  }



  /**
    * creates a string to represent edge ids and their flows
    * @param graph
    * @return
    */
  def stringEncodeFlowSnapshot(graph: LocalGraph): String = {
    graph.edges.map {
      e =>
        s"${e._1}${Relations.flow}${e._2.attribute.flow.getOrElse(0D)}"
    }.mkString(Relations.flowSnapshot, Relations.edge, "")
  }

  /**
    * decodes a string representation of the graph flows into a map of EdgeId -> Flow
    * @param str the encoded flow snapshot
    * @return data used to update a graph with the correct flow values
    */
  def stringDecodeFlowSnapshot(str: String): Map[String, Int] = {
    str
      .drop(Relations.flowSnapshot.length)
      .split(Relations.edge)
      .map {
        tupleRaw =>
          val tupleSplit = tupleRaw.split(Relations.flow)
          val (edge, flow) = (tupleSplit(0), tupleSplit(1))
          edge -> flow.toDouble.toInt
      }.toMap
  }


  /**
    * encodes a routing request as a string
    * @param obj a routing request
    * @return serialized request
    */
  def stringEncodeRequest(obj: GenMap[LocalODPair, GenSeq[Path]]): String = {
    obj.map {
      personTuple =>
        val (personId, odSrc, odDst) = (personTuple._1.id, personTuple._1.src, personTuple._1.dst)
        val altPaths: String = personTuple._2.map {
          altPath =>
            altPath.map {
              seg =>
                val segId = seg.edgeId
                val segCost = seg.cost match {
                  case None => Relations.none
                  case Some(cost) => cost.mkString(Relations.cost)
                }
                s"$segId${Relations.costs}$segCost"
            }.mkString(Relations.segment)
        }.mkString(Relations.alt) // some kind of seg type
        s"$personId${Relations.src}$odSrc${Relations.dst}$odDst${Relations.altPaths}$altPaths"
    }.mkString(Relations.request, Relations.person,"")
  }

  val LocalODPairRegex: Regex = "(.+)%SRC%(.+)%DST%(.+)".r


  /**
    * decodes a routing request that was serialized
    * @param str a stringified routing request
    * @return the original request
    */
  def stringDecodeRequest(str: String): GenMap[LocalODPair, GenSeq[Path]] =
    str
      .drop(Relations.request.length)
      .split(Relations.person)
      .map {
        person =>
          val personSplit = person.split(Relations.altPaths)

          val localODPairRaw = personSplit(0)
          val localODPair: LocalODPair = localODPairRaw match {
            case LocalODPairRegex(id, src, dst) =>
              LocalODPair(id, src, dst)
            case x =>
              throw new IllegalStateException(s"LocalODPair unable to deserialize: $x didn't match pattern $LocalODPairRegex")
          }

          val altPaths: Seq[Path] =
            if (personSplit.length == 1) Seq()
            else {
              personSplit(1)
                .split(Relations.alt)
                .map {
                  alt =>
                    alt
                      .split(Relations.segment)
                      .map {
                        seg =>
                          val segSplit = seg.split(Relations.costs)
                          val (segmentId, costsRaw) = (segSplit(0), segSplit(1))
                          val costs =
                            costsRaw
                              .split(Relations.cost)
                              .toList
                              .map{_.toDouble} match {
                              case Nil => None
                              case xs => Some(xs)
                            }

                          SORoutingPathSegment(segmentId, costs)
                      }.toList
                }.toVector
            }

          localODPair -> altPaths
      }.toMap


  /**
    * a cheap way to deal with serialization, as a replacement for twitter-chill on Scala object serialization. this stays java-friendly.
    * @param obj
    * @return
    */
  def stringEncodeResult(obj: PedrosoReiSparkLivyResult): String = {
    val bestPaths = obj.bestSimulation.map{_.toString}.mkString(Relations.tag)
    val bestSimCost = obj.bestCost.toString
    val logs = obj.logs.mkString(Relations.log)
    s"$bestPaths${Relations.entry}$bestSimCost${Relations.entry}$logs"
  }

  /**
    * a cheap way to deal with serialization, pt 2.
    * @param str
    * @return
    */
  def stringDecodeResult(str: String): PedrosoReiSparkLivyResult = {
    val input = str.split(Relations.entry)
    val bestPaths: Seq[Tag] =
      if (input(0).isEmpty) Seq()
      else input(0)
        .split(Relations.tag)
        .map {
          t =>
            val tag = t.split(Tag.toStringSeparator)
            Tag(tag(0), tag(1).toInt)
        }
    val bestSimCost: BigDecimal = BigDecimal(input(1))
    val logs: Map[String, String] =
      input(2)
        .split(Relations.log)
        .map {
          entry =>
            val kv = entry.split(Relations.kv) // symbol in Map toString to represent k->v relation
            kv(0) -> kv(1)
        }.toMap
    PedrosoReiSparkLivyResult(bestSimCost, bestPaths, logs)
  }
}
