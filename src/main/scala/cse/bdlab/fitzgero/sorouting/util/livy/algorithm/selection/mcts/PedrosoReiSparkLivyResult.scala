package cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts

import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.Tag
import cse.bdlab.fitzgero.sorouting.model.path.SORoutingPathSegment
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.local.LocalODPair

// WARNING! changes to this file need to be updated in the separate "SO-Routing-Models" package and resulting .jar file stored in lib/

case class PedrosoReiSparkLivyResult (
  bestCost: BigDecimal,
  bestSimulation: Seq[Tag] = Seq(),
  //  bestSimulation: Map[LocalODPair, List[SORoutingPathSegment]] = Map(),
  logs: Map[String,String] = Map(),
  iterations: Long = 0L
) extends Serializable {
  //  def hasSolution: Boolean = bestSimulation.nonEmpty
  def hasSolution: Boolean = bestSimulation.nonEmpty
}