package cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.iterative

import cse.bdlab.fitzgero.mcts.algorithm.samplingpolicy.scalar.UCTScalarPedrosoReiReward.Coefficients
import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.Tag

case class AggregatePayload (
  payloadState  : PayloadState,
  c             : Coefficients,
  reward        : Double,  // an "anytime evaluation" of the reward, aka, with Cp == 0.0D
  bestSolution  : Tag.AlternatesSet,
  isTerminal    : Boolean = false,
  mctsIterations: Long = 0,
  runtime       : List[Long] = Nil) extends Product with Serializable {

  override def toString: String = {
    val nodeType = if (isTerminal) "leaf" else "brch"
    s"AggregatePayload($nodeType, reward: $reward, best solution: ${IterativeLivyOps.easyDebugAlternates(bestSolution)}, iters: $mctsIterations, isTerminal: $isTerminal)\n ⌞-> with $payloadState\n"
  }

}