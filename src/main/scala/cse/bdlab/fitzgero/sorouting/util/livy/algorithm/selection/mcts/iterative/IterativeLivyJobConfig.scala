package cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.iterative

case class IterativeLivyJobConfig(
  executors: Int,
  partitions: Int,
  uctThreshold: Double = 0.0D,
  payloadsPerPartition: Int = 100,
  attemptPolicy: Int = 3,
  rootIterations: Int = 500,
  nodeIterations: Int = 50
)

