package cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.iterative

import scala.annotation.tailrec
import scala.collection.{GenMap, GenSeq}

import cse.bdlab.fitzgero.mcts.algorithm.samplingpolicy.scalar.UCTScalarPedrosoReiReward.Coefficients
import cse.bdlab.fitzgero.mcts.tree.MCTreePedrosoReiReward
import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.PedrosoReiSparkMonolithicAlgorithm.Path
import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.Tag
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.local.LocalODPair

object IterativeLivyOps {

  type Tree = MCTreePedrosoReiReward[Tag.AlternatesSet, Tag]

  /**
    * utility function that removes children that don't meet our threshold for exploration
    * @param tree parent node
    * @param c the most current coefficients we have
    * @param uctThreshold the reward threshold we will use to filter, by default accepts all children
    * @return children that meet the threshold
    */
  def childrenToKeep(tree: Tree, c: Coefficients, uctThreshold: Double = Double.MinValue): List[Tree] = {
    if (uctThreshold == 0D) tree.childrenNodes.values.toList
    else tree.childrenNodes.values.filter{_.reward(c) > uctThreshold}.toList
  }

  /**
    * utility function that, given a list of alt path counts, tells us how many alt paths the next agent should have
    * @param counts a map from driver number (corresponding to our Request) to number of alt paths
    * @param state the current state of m drivers, where we want to know the number of alts of driver m+1
    * @return number of child alt paths
    */
  def numChildrenHelper(counts: GenMap[Int, Int])(state: Tag.AlternatesSet): Int = {
    if (state.size >= counts.size) 0
    else counts(state.size)
  }


  /**
    * when we know the next agent has only one alt, we can simply construct that selection and bypass a search
    * @param requests the original request object
    * @param state the current state
    * @return the current state of m selections with m+1'th selection constructed/added
    */
  def selectOnlyOptionFor(requests: GenMap[LocalODPair, GenSeq[Path]])(state: Tag.AlternatesSet): Tag.AlternatesSet = {
    state :+ Tag(requests.drop(state.size).head._1.id, 1)
  }


  /**
    * constructs a list, telling the number of alternate paths for each request in our batch
    * @param request the request batch
    * @return a list of numbers, each representing the number of alternate paths per request
    */
  def altCounts(request: GenMap[LocalODPair, GenSeq[Path]]): List[Int] = request.zipWithIndex.map { _._1._2.size }.toList


  /**
    * decides the depth (i.e. size of request to build all combinations) of the search space to construct a frontier
    * @param altCounts a list of the count of alternate paths per request, in the same order as the request object
    * @param executors number of executors in our cluster
    * @param partitions number of partitions per executor
    * @param payloads number of payloads per partition
    * @return the depth, or size of the request we will take to construct all combinations of alternates for those requests taken
    */
  def frontierDepth(altCounts: List[Int], executors: Int, partitions: Int, payloads: Int): Int = {
    val payloadBudget = executors * partitions * payloads

    @tailrec
    def _frontierOf(remainingCounts: List[Int], acc: Int = 1, depth: Int = 0): Int =
      if (remainingCounts.isEmpty || acc * remainingCounts.head > payloadBudget) depth
      else _frontierOf(remainingCounts.tail, acc * remainingCounts.head, depth + 1)

    _frontierOf(altCounts)
  }


  /**
    * guards against selecting the leaf depth for the frontier depth - pulls it back to the parents of the leaves
    * @param frontierDepth the frontier depth as calculated with respect to our payload budget
    * @param requestSize the size of the batch request
    * @return a corrected frontierDepth
    */
  def preventLeafDepth(frontierDepth: Int, requestSize: Int): Int = if (frontierDepth >= requestSize) requestSize - 1 else frontierDepth


  /**
    * pads the combinations with repeats until each partition has padAmount payloads
    * @tparam T AggregatePayload, or any (for testing)
    * @param combinations the frontier combinations generated as a function of our frontierDepth calculation
    * @param executors the number of executors in our cluster
    * @param partitions the number of partitions per executor
    * @param padAmount the number of payloads we want per partition in our frontier
    * @return
    */
  def fillPartitionBudget[T](combinations: Seq[T], executors: Int, partitions: Int, padAmount: Int = 1): Seq[T] = {
    val cutoff = executors * partitions * padAmount

    @tailrec
    def _fill(xs: Seq[T]): Seq[T] =
      if (xs.size > cutoff) xs.take(cutoff)
      else _fill(xs ++ xs)

    _fill(combinations)
  }

  /**
    * given a set of requests, produce the set of all possible combination states
    * @param req a set of requests
    * @return a list of possible search States
    */
  def frontierCombinations(req: GenMap[LocalODPair, GenSeq[Path]]): List[List[Tag]] = {
    val tags: List[List[Tag]] = {
      for {
        request <- req
        alt <- request._2.zipWithIndex
      } yield Tag(request._1.id, alt._2)
    }.toList
      .groupBy(_.personId)
      .values
      .toList

    def _combs(search: List[List[Tag]], sol: List[Tag] = List()): List[List[Tag]] = {
      if (search.isEmpty) List(sol)
      else {
        val recurse = for {
          selection <- search.head
        } yield _combs (search.tail, sol :+ selection)
        recurse.flatten
      }
    }
    _combs(tags)
  }

//  def takeBest(xs: Iterator[AggregatePayload]): Iterator[AggregatePayload] = {
//    xs.toArray.sortBy(_.payloadState.localBestSimulation)
//  }

  /**
    * converts a list of tags into a simple representation, based on index # and alt #. note, it does not actually review the tag name, and assumes the tags presented are in the correct order.
    * @param xs a state object
    * @return simple representation for debugging
    */
  private[iterative] def easyDebugAlternates(xs: Tag.AlternatesSet): String =
    xs.
      zipWithIndex.
      map { x =>
        s"${x._2}-${x._1.alternate}"
      }.
      mkString(",")

  /**
    * helper to prevent silly mistakes, mostly
    * @param runTimeList the collection of runtime calculations
    * @param startTime the time that we are calculating against for this addition
    * @return the runTimeList with an added element, representing the current time minus the startTime
    */
  def calculateThisRuntime(runTimeList: List[Long], startTime: Long): List[Long] = runTimeList :+ (System.currentTimeMillis - startTime)


  /**
    * estimates the number of mcts iterations. assumes total executor/partition/payload utility at each algorithm step, and no searches were cut off by the time budget
    * @param rootNodeVisits the number of iterations of mcts on the root node
    * @param frontierIters from the "best" search node, the number of iterations
    * @param mctsItersPerStep the user-set number of mcts iterations per algorithm step
    * @param stepsTaken the number of cycles of flatMap/mapPartitions that occured
    * @param totalPayloads the value of executors * partitions * payloads
    * @return the estimated number of mcts iterations across the search
    */
  def estimatedTotalNodeIterations(rootNodeVisits: Long, frontierIters: Long, mctsItersPerStep: Long, stepsTaken: Int, totalPayloads: Int): Long = {
    rootNodeVisits + ((frontierIters - rootNodeVisits) * (mctsItersPerStep * stepsTaken) * totalPayloads)
  }

}

//  SizeEstimator can tell us how big a data structure is in Spark
//
//        val estimator = org.apache.spark.util.SizeEstimator
//        println(s"size of topPayload tree:  ${estimator.estimate(topSearch)}")
//        val topPayload = topSearch.map { tree =>
//          AggregatePayload(PayloadState(tree.state, tree.averageSum, tree.bestSimulation, tree.visits), topC, tree.reward(topC), false, root.visits)
//        }

//        println(s"size of graph:  ${estimator.estimate(graph)}")
//        println(s"size of request:  ${estimator.estimate(request)}")
//        println(s"~~~~~ running new search with topPayload:")
//        println(topPayload)
//        println(s"  ~~~ stopping criteria")
//        println(s"      iterations per search: $iterationsPerSearch")
//        println(s"      noLuckThreshold: $noLuckIterationsThreshold")

//        var frontier: RDD[AggregatePayload] = sc.parallelize(topPayload, numSparkExecutors * numPartitions).cache()
