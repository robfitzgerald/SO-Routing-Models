package cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.iterative

import scala.annotation.tailrec
import scala.collection.{GenMap, GenSeq}
import scala.xml.XML

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

import cse.bdlab.fitzgero.mcts.algorithm.samplingpolicy.banditfunction.UCT_PedrosoRei.Minimize
import cse.bdlab.fitzgero.mcts.algorithm.samplingpolicy.scalar.UCTScalarPedrosoReiReward.Coefficients
import cse.bdlab.fitzgero.mcts.core.terminationcriterion._
import cse.bdlab.fitzgero.mcts.tree.MCTreePedrosoReiReward
import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.PedrosoReiSparkMonolithicAlgorithm.Path
import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.{RoutingSelectionPedrosoReiMCTSSolver, Tag}
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.costfunction.BPRCostFunctionType
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.local.LocalGraphOps.EdgesWithFlows
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.local.{LocalGraph, LocalGraphOps, LocalODPair}
import cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.iterative.IterativeLivyOps._
import cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.{HackSerializer, PedrosoReiSparkLivyResult, ScalaXMLIgnoreDTD}
import org.apache.livy.scalaapi.{LivyScalaClient, ScalaJobHandle}

// note: three pruning stages
//   1) children don't meet uct threshold
//   2) node doesn't produce children within retry policy
//   3) limited partition size, sort and xs.take(numPayloadsPerPartition)


/**
  * @param client the client used to remote submit jobs over RPC
  * @param _matsimDataRepositoryDirectory the root directory, likely in hadoop, where we can find network data
  * @param _sourceAssetsDirectory the network data we wish to use
  * @param _graph the road network to solve over
  * @param _request the set of drivers and their alternate paths
  * @param _seed a random seed value. all sub tasks will take random seeds which will be seeded by this value
  * @param _duration train time per task per worker
  * @param _timeWindow the test bed algorithm time window, which is needed when we deserialize the network graph
  */
class IterativeLivyJob (
  val client: LivyScalaClient,
  val _matsimDataRepositoryDirectory: String,
  val _sourceAssetsDirectory: String,
  val _graph: LocalGraph,
  val _request: GenMap[LocalODPair, GenSeq[Path]],
  val _seed: Long,
  val _duration: Long,
  val _timeWindow: Int,
  val conf: IterativeLivyJobConfig) {

  /**
    * workers begin MCTS at a partially complete state and reduce to find the best amongst the set
    * @return data representing the best MCTS result found by all tree searches
    */
  def run(): ScalaJobHandle[String] = {

    // temporary solution to bring these values into the direct outer scope (req'd by Livy)
    val timeWindow = _timeWindow
    val requestString: String = HackSerializer.stringEncodeRequest(_request)
    val flows: String = HackSerializer.stringEncodeFlowSnapshot(_graph)
    val sourceAssetDirectory: String = _sourceAssetsDirectory
    val matsimDataRepositoryDirectory: String = _matsimDataRepositoryDirectory
    val duration = _duration
    val seed = _seed
    val uctThreshold: Double = conf.uctThreshold
    val numSparkExecutors: Int = conf.executors
    val numPartitions: Int = conf.partitions
    val retryPolicy: Int = conf.attemptPolicy
    val maxPayloadsPerPartition: Int = conf.payloadsPerPartition // used for pruning step
    val payloadsInTopFrontier: Int = maxPayloadsPerPartition // used to decide how full the topFrontier should be. maybe a different number?
    val fixedRootIterations = conf.rootIterations
    val fixedNodeIterations = conf.nodeIterations

    client.submit[String] {
      ctx =>
        val sc: SparkContext = ctx.sc
        val stopTime: Long = System.currentTimeMillis + duration

//        println(s"[Spark-Iter02] beginning with parameters:")
//        println(s"[Spark-Iter02]   uctThreshold: $uctThreshold")
//        println(s"[Spark-Iter02]   maxPayloadsPerPartition: $maxPayloadsPerPartition")
//        println(s"[Spark-Iter02]   retryPolicy: $retryPolicy")
//        println(f"[Spark-Iter02] with remaining time budget ${(stopTime - System.currentTimeMillis).toDouble / 1000}%.2f secs")

        // instantiate a graph with the current network state
        // ignoring DTDs here due to path/cwd instability when shipped to Livy
        // DTD is confirmed in testbed, so this should be safe
        // TODO: replace "data/rye" with the config setting for sourceDirectory

        val networkXmlRaw: String = sc.wholeTextFiles(matsimDataRepositoryDirectory + sourceAssetDirectory + "/network.xml").collect()(0)._2
        val networkXML: xml.Elem = XML.withSAXParser(ScalaXMLIgnoreDTD.parser).loadString(networkXmlRaw)
        val graphNoFlows = LocalGraphOps.readMATSimXML(EdgesWithFlows, networkXML, None, BPRCostFunctionType, timeWindow)
        val edgesAndFlows = HackSerializer.stringDecodeFlowSnapshot(flows)
        val graph: LocalGraph = LocalGraphOps.updateGraph(edgesAndFlows, graphNoFlows)

        // instantiate a request
        val request: GenMap[LocalODPair, GenSeq[Path]] = HackSerializer.stringDecodeRequest(requestString)

        // our stopping criteria parameters
        // each tree search should stop if it runs for the number of iterations we are giving them
        // but they should also terminate if our global time budget is reached
        // if we have been stuck on a node for too long with no results, we cancel the search on that root

        val largestSubsetSize: Int = request.map{_._2.size}.max

        if (largestSubsetSize == 1) {
//          println(s"[Spark-Iter02] largest k=1 is equivalent to selfish case, returning no solution")
          val result = PedrosoReiSparkLivyResult(bestCost = BigDecimal(0), logs = Map("algorithm.selection.sparkiter.success" -> "0"))
          HackSerializer.stringEncodeResult(result)
        } else {

          val iterationsPerSearch: Int = fixedNodeIterations // (largestSubsetSize * largestSubsetSize * certainty).toInt
          val retryIterationsLimit: Int = fixedRootIterations + (fixedNodeIterations * retryPolicy) // iterationsPerSearch * retryPolicy
          val totalPayloadBudget: Int = numSparkExecutors * numPartitions * maxPayloadsPerPartition

          // explore and produce a decent hint at some global best/worst values
          val rootIterations = fixedRootIterations // i like the idea of req^3: request.size * request.size * request.size
          val rootTermination: IterationTermination[Tag.AlternatesSet, Tag, MCTreePedrosoReiReward[Tag.AlternatesSet, Tag]] =
            IterationTermination(rootIterations)
          val topSolver: RoutingSelectionPedrosoReiMCTSSolver = RoutingSelectionPedrosoReiMCTSSolver(graph, request, seed, rootTermination, Seq())
          val topRoot = topSolver.run()
          val topCoefficients = topSolver.getSearchCoefficients(topRoot)


          // find the search frontier depth that we can fit in our budget, to build our top frontier
          val topFrontierDepth =
            preventLeafDepth(
              frontierDepth = frontierDepth(
                altCounts = altCounts(request),
                executors = numSparkExecutors,
                partitions = numPartitions,
                payloads = payloadsInTopFrontier),
              requestSize = request.size
            )
          val frontierBuildMaxDepth: Int = request.size - topFrontierDepth

          // build the top frontier by creating all combinations and padding by repetition until we fill all partitions
          val allCombinationsPadded: Seq[AggregatePayload] = {
            val allCombs = frontierCombinations(request.take(topFrontierDepth))
              .map { state =>

                AggregatePayload(
                  payloadState = PayloadState(
                    treeState = state,
                    localAverageSum = BigDecimal(0),
                    localBestSimulation = Minimize().defaultSimulation,
                    visits = 0L
                  ),
                  c = topCoefficients,
                  reward = 0D,
                  bestSolution = topSolver.bestSolution,
                  isTerminal = state.size == request.size,
                  mctsIterations = topRoot.visits
                )
              }
            // repeat allCombs until we fill our topFrontier budget


            fillPartitionBudget(allCombs, numSparkExecutors, numPartitions, payloadsInTopFrontier)
          }

          println(s"[Spark-Iter02] request ${altCounts(request)}, topFrontier of size ${allCombinationsPadded.size} constructed from depth $topFrontierDepth, attempting to run $frontierBuildMaxDepth iterations")
          println(s"[Spark-Iter02] Top Coefficients: ${topSolver.getSearchCoefficients(topRoot)}")

          val topFrontier: RDD[AggregatePayload] = sc.parallelize(allCombinationsPadded, numSparkExecutors * numPartitions)

          // broadcasting the shared data resources
          val graphBroadcast = sc.broadcast(graph)
          val requestBroadcast = sc.broadcast(request)
          val seedBroadcast = sc.broadcast(seed)
          val stopTimeBroadcast = sc.broadcast(stopTime)
          val iterationsBroadcast = sc.broadcast(iterationsPerSearch)
          val retryIterationsLimitBroadcast = sc.broadcast(retryIterationsLimit)
          val thresholdUCTBroadcast = sc.broadcast(uctThreshold)

          println(f"[Spark-Iter02] done with setup with remaining time budget ${(stopTime - System.currentTimeMillis).toDouble / 1000}%.2f secs")


          @tailrec
          def runIterativeMCTS(
            globalTimeBudget: TimeTermination[Tag.AlternatesSet, Tag, MCTreePedrosoReiReward[Tag.AlternatesSet, Tag]],
            frontier: RDD[AggregatePayload],
            frontierBuildIteration: Int = 1): (RDD[AggregatePayload], Int) = {
            if (frontierBuildIteration > frontierBuildMaxDepth) {
              (frontier, frontierBuildIteration)
            } else {

              // run a tree search on the frontier, which is all (non-pruned) tree nodes of the same tree depth
              val iterationResult: RDD[AggregatePayload] =
                frontier
                  .flatMap (
                    payload => {
                      val flatMapStartTime: Long = System.currentTimeMillis

                      if (payload.isTerminal) {
                        // leaf node. no search.
                        List(payload.copy(runtime = calculateThisRuntime(payload.runtime, flatMapStartTime)))

                        // this is a nice, fancy optimization but may be messing things up
//                        else if (childNodesOfThisPayload == 1) {
//                          // skip this iteration if there is only one actual choice (an alternate paths set of 1 item)
//                          // select the only option and move on without running MCTS search
//                          val addOnlyOptionToState = selectOnlyOptionFor(requestBroadcast.value)(payload.payloadState.treeState)
//                          List(
//                            payload.copy(
//                              payloadState = payload.payloadState.copy(treeState = addOnlyOptionToState),
//                              runtime = payload.runtime :+ flatMapStartTime - System.currentTimeMillis)
//                          )
//                        }

                      } else {
                        // our termination criteria is to add "iterationsPerSearch" iterations more to this root node,
                        // unless we run out of time.
                        // calculated dynamically
                        val stoppingIterations: Long = payload.payloadState.visits + iterationsBroadcast.value // payload.payloadState.visits + iterationsBroadcast.value * (requestBroadcast.value.size - payload.payloadState.treeState.size)
                        val terminationCriterion: CombinedTermination[Tag.AlternatesSet, Tag, MCTreePedrosoReiReward[Tag.AlternatesSet, Tag]] =
                          CombinedTermination.Predef.TimeAndIteration(stopTimeBroadcast.value, stoppingIterations)

                        //                  println(s"~~~~~ executor payload:")
                        //                  println(payload)
                        //                  println(s"payload state visits: ${payload.payloadState.visits} iteration budget: ${iterationsBroadcast.value} will stop at: $stoppingIterations")
                        //                  val currentTime = System.currentTimeMillis
                        //                  println(s"current time: $currentTime stopping time: ${stopTimeBroadcast.value} remainingDur: ${stopTimeBroadcast.value - currentTime}")

                        val solver = RoutingSelectionPedrosoReiMCTSSolver(
                          graph = graphBroadcast.value,
                          request = requestBroadcast.value,
                          seed = seedBroadcast.value, // pass a seed in via payload?
                          terminationCriterion = terminationCriterion
                        )

                        // the solver needs to get the global best/worst from the latest synchronize step
                        solver.globalBestSimulation = payload.c.globalBestSimulation
                        solver.globalWorstSimulation = payload.c.globalWorstSimulation
                        solver.bestSolution = payload.bestSolution

                        // rehydrate the tree of this child
                        val tree: MCTreePedrosoReiReward[Tag.AlternatesSet, Tag] =
                          MCTreePedrosoReiReward(
                            state = payload.payloadState.treeState,
                            action = None,
                            objective = Minimize(payload.c.globalBestSimulation, payload.c.globalWorstSimulation)
                          )
                        tree.averageSum = payload.payloadState.localAverageSum
                        tree.bestSimulation = payload.payloadState.localBestSimulation
                        tree.visits = payload.payloadState.visits

                        // run this search
                        val searchResult = solver.run(tree)

//                        println(s"  ~~~ search complete. this node has ${searchResult.visits} visits starting from state ${searchResult.state}. result: ")
//                        println(s"treeBestSim: ${searchResult.bestSimulation} solverBestSolution: ${solver.bestSolution}")
//                        println(s"globalTimeBudget says -> withinComputationalBudget? ${globalTimeBudget.withinComputationalBudget(searchResult)}")
//                        println(s"ran until ${searchResult.visits} visits and until ${System.currentTimeMillis}")

                        // update coefficients based on this search
                        val c: Coefficients = Coefficients(
                          payload.c.Cp,
                          solver.globalBestSimulation, // for now, this is all side-effect city yo
                          solver.globalWorstSimulation
                        )

                        val iterationsDiff: Long = searchResult.visits - payload.payloadState.visits
                        val mctsIterationsUpdate: Long = payload.mctsIterations + iterationsDiff

                        // find the children that are above our threshold in quality, prune the rest
                        val goodChildren: List[AggregatePayload] =
                          childrenToKeep(searchResult, c, thresholdUCTBroadcast.value)
                            .map{
                              child =>
                                val isTerminal = child.state.size == requestBroadcast.value.size

                                // the payload state will have the best simulation reward and selection of it's parent.
                                // .. is this correct? well, it removes any knowledge about the child we had, for one,
                                //    but
                                //      if we kept the solver.bestSolution with the child.bestSimulation, those values would not be correct
                                //      getting the best solution of the child would
                                //        require a rewrite
                                //        take up a lot of space
                                //        not necessarily help, since the parent is the one who had the most samples and had the broadest perspective on reward distributions
                                //    also, i think this might help us prune, if we rely on the parent's bestSimulation
                                // previous: PayloadState(child.state, child.averageSum, child.bestSimulation, <nothing here>, child.visits)

                                val payloadState =
                                  PayloadState(
                                    treeState = child.state,
                                    localAverageSum = child.averageSum,
                                    localBestSimulation = child.bestSimulation,
                                    visits = child.visits
                                  )

                                AggregatePayload(
                                  payloadState = payloadState,
                                  c = c,
                                  mctsIterations = mctsIterationsUpdate,
                                  reward = child.reward(c.copy(Cp = 0D)),
                                  bestSolution = solver.bestSolution,
                                  isTerminal = isTerminal
                                )
                            }

                        //                  println("  ~~~ goodChildren from this payload:")
                        //                  println(goodChildren)

                        // next payload is our good children, but
                        // if none are found, let's stay on this root for another phase
                        // UNLESS we have visited it enough times to be confident in leaving it behind
                        val nextPayload: List[AggregatePayload] = {
                          if (goodChildren.nonEmpty) {

                            goodChildren.map{
                              _.copy(runtime = calculateThisRuntime(payload.runtime, flatMapStartTime))
                            }
                          } else {
                            // we have no children
                            // this can be the case of being a leaf node, which is handled below in the "else" branch
                            // but, it can also be one of two cases which we would like to prune:
                            //   if we have never visited this node, then it was a topFrontier node, and we want to get rid of it
                            //   if it isn't a leaf node, we want to consider it for our retryPolicy, which allows for retrying nodes up to some depth
                            if (searchResult.visits == 0 || !payload.isTerminal && mctsIterationsUpdate > retryIterationsLimitBroadcast.value)
                              Nil
                            else {
                              List(
                                payload.copy(
                                  payloadState = PayloadState(searchResult.state, searchResult.averageSum, searchResult.bestSimulation, searchResult.visits),
                                  c = c,
                                  mctsIterations = mctsIterationsUpdate,
                                  reward = searchResult.reward(c.copy(Cp = 0D)), // allow for anytime evaluation of the reward, not considering exploration,
                                  bestSolution = solver.bestSolution,
                                  runtime = calculateThisRuntime(payload.runtime, flatMapStartTime)
                                )
                              )
                            }
                          }
                        }

                        // return
                        nextPayload
                      }
                    }
                  )
                  .mapPartitions { partition =>
                    val mapPartitionsStartTime = System.currentTimeMillis
                    partition
                      .toVector
                      .sortBy(c => c.payloadState.localBestSimulation) // * c.payloadState.visits) // rewards biased by # of visits, for times when the algorithm ends while still iterating on leaves.
                      .toIterator
                      .take(maxPayloadsPerPartition)
                      .map{p => p.copy(runtime = p.runtime :+ (System.currentTimeMillis - mapPartitionsStartTime))}
                  }

//              println(f"[Spark-Iter02] added iteration $iteration of execution plan with remaining time budget ${(stopTime - System.currentTimeMillis).toDouble / 1000}%.2f secs")
              runIterativeMCTS(globalTimeBudget, iterationResult, frontierBuildIteration + 1)

            }
          }

          // run iterative algorithm
          val (largeResultRDD, algorithmIterations) = runIterativeMCTS(
            TimeTermination[Tag.AlternatesSet, Tag, MCTreePedrosoReiReward[Tag.AlternatesSet, Tag]](stopTime),
            topFrontier
          )

          println(f"[Spark-Iter02] finished building tree search execution plan with remaining time budget ${(stopTime - System.currentTimeMillis).toDouble / 1000}%.2f secs")


          ///////// RUN SPARK JOB /////////////////////////////////
          // cached final product has 1 best solution per partition
          // note: partitions are already sorted at this point

          val bestFromEachPartition = largeResultRDD.mapPartitions{_.take(1)}.cache()



          println(f"[Spark-Iter02] final flatmap added with remaining time budget ${(stopTime - System.currentTimeMillis).toDouble / 1000}%.2f secs")

          // isEmpty triggers computation of entire execution plan
          // the RDD will be empty only if we do not reach the leaves within our computation budget
          val resultIsEmpty = bestFromEachPartition.isEmpty
          println(f"[Spark-Iter02] algorithm run (@ bestFromEachPartition.isEmpty) with remaining time budget ${(stopTime - System.currentTimeMillis).toDouble / 1000}%.2f secs")

          val finishTime: Long = System.currentTimeMillis
          val overBudget = if (finishTime > stopTime) finishTime - stopTime else 0L



          val best: AggregatePayload = if (resultIsEmpty) {
            // we now return the root from our topSearch here in the case of an empty RDD,
            // which has a bestSolution resulting from topRoot.visits iterations.

            AggregatePayload(
              payloadState = PayloadState(
                treeState = topRoot.state,
                localAverageSum = topRoot.averageSum,
                localBestSimulation = topRoot.bestSimulation,
                visits = topRoot.visits
              ),
              c = topCoefficients,
              reward = 0D,
              bestSolution = topSolver.bestSolution,
              mctsIterations = topRoot.visits
            )

          } else {
            implicit val treeOrdering: Ordering[AggregatePayload] = Ordering.by(_.payloadState.localBestSimulation)
            bestFromEachPartition.min
          }

          val (bestCost, bestSolution) = {
            if (best.payloadState.localBestSimulation < topRoot.bestSimulation) {
              (best.payloadState.localBestSimulation, best.bestSolution)
            } else {
              (topSolver.globalBestSimulation, topSolver.bestSolution)
            }
          }

          val result = PedrosoReiSparkLivyResult(bestCost, bestSolution, Map())


          // logging
          val bestFoundInIterativeSearch: Int = if (best.payloadState.localBestSimulation != topRoot.bestSimulation) 1 else 0
          val flatMapRuntime: Long = if (best.runtime.nonEmpty) best.runtime.sliding(1,2).flatten.sum else 0L
          val mapPartitionsRuntime: Long = if (best.runtime.size > 2) best.runtime.tail.sliding(1,2).flatten.sum else 0L
//          val distanceFromLeaves: Int = request.size - best.payloadState.treeState.size
          val distanceExplored: Int = best.payloadState.treeState.size - topFrontierDepth
          val singleBranchIterationsEstimate: Int = best.runtime.count(_ > 1L) / 2
          val totalIterationsEstimate: Long = estimatedTotalNodeIterations(topRoot.visits, best.mctsIterations, fixedNodeIterations, singleBranchIterationsEstimate, totalPayloadBudget)

          val finalResult = result.copy(
            logs =
              result.logs
                .updated("algorithm.selection.sparkiter.success", "1")
                .updated("algorithm.selection.sparkiter.iterations.estimate", totalIterationsEstimate.toString)
                .updated("algorithm.selection.sparkiter.complete", if (best.isTerminal) "1" else "0")
                .updated("algorithm.selection.sparkiter.iterations", algorithmIterations.toString)
                .updated("algorithm.selection.sparkiter.overtime", overBudget.toString)
                .updated("algorithm.selection.sparkiter.best.found.in.iterative.search", bestFoundInIterativeSearch.toString)
                .updated("algorithm.selection.sparkiter.runtime.searchstep", flatMapRuntime.toString)
                .updated("algorithm.selection.sparkiter.runtime.prunestep", mapPartitionsRuntime.toString)
          )
          if (finishTime > stopTime) {
            println(f"[Spark-Iter02] returning result ${overBudget.toDouble / 1000}%.2f secs after budget completion")
          }

          println(s"[Spark-Iter02] Estimated total MCTS Iterations in this run: $totalIterationsEstimate")
          println(s"[Spark-Iter02] Best from search:\n$best")
          println(s"[Spark-Iter02] Best was a terminal leaf: ${best.isTerminal}; best was found in iterative search: ${if (bestFoundInIterativeSearch == 1) "yes" else "no"}")
//          println(f"[Spark-Iter02] Best improvement over topSearch: ${1 - (best.payloadState.localBestSimulation / topRoot.bestSimulation)}%.2f %%")
          println(s"[Spark-Iter02] Time Best node spent in search phase: ${flatMapRuntime.toDouble / 1000} secs; prune phase: ${mapPartitionsRuntime.toDouble / 1000} secs over $distanceExplored algorithm iterations.")
          println("\n\n")


          HackSerializer.stringEncodeResult(finalResult)
        }
    }
  }
}

