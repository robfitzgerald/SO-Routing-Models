package cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.iterative

import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.Tag

case class PayloadState (
  treeState          : Tag.AlternatesSet,
  localAverageSum    : BigDecimal,
  localBestSimulation: BigDecimal,
  visits             : Long) extends Product with Serializable {

  /**
    * localAverageSum becomes the average when we divide it by the number of visits
    * @return
    */
  def localAvg: BigDecimal = if (visits == 0L) BigDecimal(0) else localAverageSum/visits

  override def toString: String = s"PayloadState(${IterativeLivyOps.easyDebugAlternates(treeState)}, localAvg: ${localAvg.toString}, localBestSim: $localBestSimulation, visits: $visits)"

}
