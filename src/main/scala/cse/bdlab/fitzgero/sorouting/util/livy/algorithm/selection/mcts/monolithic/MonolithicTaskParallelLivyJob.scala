package cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.monolithic

import scala.collection.{GenMap, GenSeq}
import scala.xml.XML

import org.apache.spark.SparkContext

import cse.bdlab.fitzgero.mcts.core.terminationcriterion.TimeTermination
import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.PedrosoReiSparkMonolithicAlgorithm.Path
import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.{RoutingSelectionPedrosoReiMCTSSolver, Tag}
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.costfunction.BPRCostFunctionType
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.local.LocalGraphOps.EdgesWithFlows
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.local.{LocalGraph, LocalGraphOps, LocalODPair}
import cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.{HackSerializer, PedrosoReiSparkLivyResult, ScalaXMLIgnoreDTD}
import org.apache.livy.scalaapi.{LivyScalaClient, ScalaJobHandle}

// WARNING! changes to this file need to be updated in the separate "SO-Routing-Models" package and resulting .jar file stored in lib/

/**
  * @param client the client used to remote submit jobs over RPC
  * @param _matsimDataRepositoryDirectory the root directory, likely in hadoop, where we can find network data
  * @param _sourceAssetsDirectory the network data we wish to use
  * @param _graph the road network to solve over
  * @param _request the set of drivers and their alternate paths
  * @param _seed a random seed value. all sub tasks will take random seeds which will be seeded by this value
  * @param _duration train time per task per worker
  * @param _timeWindow the test bed algorithm time window, which is needed when we deserialize the network graph
  * @param _numSparkExecutors the number of cores in the cluster that are devoted to this job
  * @param _maxTasksPerWorker the number of tasks each worker can be assigned, which will limit how deep in the tree we go before applying solvers
  */
class MonolithicTaskParallelLivyJob (
  val client                        : LivyScalaClient,
  val _matsimDataRepositoryDirectory: String,
  val _sourceAssetsDirectory        : String,
  val _graph                        : LocalGraph,
  val _request                      : GenMap[LocalODPair, GenSeq[Path]],
  val _seed                         : Long,
  val _duration                     : Long,
  val _timeWindow                   : Int,
  val _numSparkExecutors            : Int,
  val _maxTasksPerWorker            : Int = 1) {



  /**
    * workers begin MCTS at a partially complete state and reduce to find the best amongst the set
    * @return data representing the best MCTS result found by all tree searches
    */
  def run(): ScalaJobHandle[String] = {

    // temporary solution to bring these values into the direct outer scope (req'd by Livy)
    val numSparkExecutors = _numSparkExecutors
    val maxTasksPerWorker = _maxTasksPerWorker
    val timeWindow = _timeWindow
    val requestString: String = HackSerializer.stringEncodeRequest(_request)
    val flows: String = HackSerializer.stringEncodeFlowSnapshot(_graph)
    val sourceAssetDirectory: String = _sourceAssetsDirectory
    val matsimDataRepositoryDirectory: String = _matsimDataRepositoryDirectory
    val duration = _duration
    val seed = _seed

    client.submit[String] {
      ctx =>
        val sc: SparkContext = ctx.sc

        val stopTime: Long = System.currentTimeMillis + duration

        // instantiate a graph with the current network state
        // ignoring DTDs here due to path/cwd instability when shipped to Livy
        // DTD is confirmed in testbed, so this should be safe
        // TODO: replace "data/rye" with the config setting for sourceDirectory

        val networkXmlRaw: String = sc.wholeTextFiles(matsimDataRepositoryDirectory + sourceAssetDirectory + "/network.xml").collect()(0)._2

        val networkXML: xml.Elem = XML.withSAXParser(ScalaXMLIgnoreDTD.parser).loadString(networkXmlRaw)
        val graphNoFlows = LocalGraphOps.readMATSimXML(EdgesWithFlows, networkXML, None, BPRCostFunctionType, timeWindow)
        val edgesAndFlows = HackSerializer.stringDecodeFlowSnapshot(flows)

        val graph: LocalGraph = LocalGraphOps.updateGraph(edgesAndFlows, graphNoFlows)

        // instantiate a request
        val request: GenMap[LocalODPair, GenSeq[Path]] = HackSerializer.stringDecodeRequest(requestString)

        // find how far to go into the tree to parallelize
        val taskParallelismThreshold: Int = numSparkExecutors * maxTasksPerWorker
        val requestDataAsSortedTags: Map[Tag.PersonID, Tag.PersonAlternatePaths] = MonolithicLivyOps.requestDataToSortedTags(request)
        val diveDepth: Int = MonolithicLivyOps.diveDepth(requestDataAsSortedTags, MonolithicLivyOps.MinimumTreeThreshold, taskParallelismThreshold)

        // a work payload contains a meta tree root (representing a partially traversed tree state), along with a random seed value for the search
        val metaTreeLeaves: Seq[Tag.AlternatesSet] = MonolithicLivyOps.metaTreeStates(requestDataAsSortedTags, diveDepth)
        val random = new scala.util.Random(seed)
        val workPayload: Seq[(Tag.AlternatesSet, Long)] = metaTreeLeaves.zip((1 to metaTreeLeaves.size).map { n => random.nextInt.toLong })

        // broadcasting the shared data resources
        val graphBroadcast = sc.broadcast(graph)
        val stopTimeBroadcast = sc.broadcast(stopTime)
        val requestBroadcast = sc.broadcast(request)

        // run parallel compute job
        val result =
          sc
            .parallelize(workPayload, numSparkExecutors)
            .aggregate(MonolithicLivyOps.DefaultResult)(
              (accumulator: PedrosoReiSparkLivyResult, payload: (Tag.AlternatesSet, Long)) => {
                println("~~ running task for payload ~~")
                println(payload)
                val startState = payload._1.toIterator.toList
                val solver = RoutingSelectionPedrosoReiMCTSSolver(
                  graphBroadcast.value,
                  requestBroadcast.value,
                  payload._2,
                  TimeTermination(stopTimeBroadcast.value),
                  startState
                )
                val tree = solver.run()
                val solution: Seq[Tag] = solver.bestSolution
                val log = Map(
                  s"state-${payload._1.map { a => s"${a.personId}#${a.alternate}" }}" -> s"iterations: ${tree.visits} cost: ${solver.globalBestSimulation.toString}"
                )

                if (solver.objective.isBetterThanOrEqualTo(solver.globalBestSimulation, accumulator.bestCost))
                  PedrosoReiSparkLivyResult(solver.globalBestSimulation, solution, log, tree.visits)
                else
                  accumulator
              },
              (a: PedrosoReiSparkLivyResult, b: PedrosoReiSparkLivyResult) => {
                val nextLogs = a.logs ++ b.logs
                val nextIterations = a.iterations + b.iterations
                if (a.bestCost < b.bestCost) a.copy(logs = nextLogs, iterations = nextIterations)
                else b.copy(logs = nextLogs, iterations = nextIterations)
              }
            )

        // package result
        val finalResult = result.copy(
          logs =
            result.logs
              .updated("height of meta tree", diveDepth.toString)
              .updated("iterations", result.iterations.toString)
        )

        HackSerializer.stringEncodeResult(finalResult)
    }
  }
}