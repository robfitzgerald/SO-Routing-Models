package cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.monolithic

import scala.annotation.tailrec
import scala.collection.{GenMap, GenSeq}
import scala.util.matching.Regex

import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.PedrosoReiSparkMonolithicAlgorithm.Path
import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.Tag
import cse.bdlab.fitzgero.sorouting.model.path.SORoutingPathSegment
import cse.bdlab.fitzgero.sorouting.model.roadnetwork.local.{LocalGraph, LocalODPair}
import cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.PedrosoReiSparkLivyResult

object MonolithicLivyOps {

  // problems that produce this few combinations will only produce a single subtree search
  val MinimumTreeThreshold: Int = 32

  // the default value for combination bestCost values for the return
  val DefaultResult: PedrosoReiSparkLivyResult = PedrosoReiSparkLivyResult(BigDecimal("9"*50))

  /**
    * finds how big our meta tree should be, by adding subsequent drivers until the number of combinations would be greater than some threshold
    * @param request the request object, representing the drivers and each of their alternate paths
    * @param minThreshold problems with fewer combinations than this number will result in a dive depth of zero (aka, don't parallelize)
    * @param maxThreshold a number that is determined based on our cluster size and a threshold of tasks per worker
    * @return the number of drivers to use to construct our meta tree
    */
  def diveDepth(request: Map[Tag.PersonID, Tag.PersonAlternatePaths], minThreshold: Int, maxThreshold: Int): Int = {
    if (request.size <= 1) 0
    else {

      val numList: List[Int] = request.map{_._2.size}.toList

      @tailrec
      def _dive(xs: List[Int], acc: Int = 1, depth: Int = 0): Int = {
        if (xs.isEmpty)
          if (acc > minThreshold) depth
          else 0
        else if (acc * xs.head >= maxThreshold) depth
        else {
          _dive(xs.tail, acc * xs.head, depth + 1)
        }
      }

      _dive(numList)
    }
  }

  /**
    * creates all possible states from a subset of driver requests, to be treated as the leaves of a meta search tree
    * @param request
    * @param diveDepth
    */
  def metaTreeStates(request: Map[Tag.PersonID, Tag.PersonAlternatePaths], diveDepth: Int): Seq[Tag.AlternatesSet] = {
    if (diveDepth == 0) Seq(Seq())
    else {
      // val alternates: GlobalAlternates = Tag.repackage(request).toList.sortBy(_._2.size).toMap
      def _combinations(xs: Tag.GlobalAlternates, partial: Tag.AlternatesSet = List()): Seq[Tag.AlternatesSet] = {
        if (xs.isEmpty || partial.size == diveDepth) List(partial)
        else {
          for {
            alt <- xs.head._2
          } yield {
            val thisTag = Tag.createTagFrom(alt._2._1, alt._1)
            _combinations(xs.tail, partial :+ thisTag)
          }
        }.flatten.toList
      }
      _combinations(request)
    }
  }


  /**
    * takes a request and sets it up for the metaTreeStates function.
    * the resulting structure is the same as the Tag.GlobalAlternates structure used internally by the Monte Carlo Tree Search implementations
    * @param request the selection request object
    * @return restructured for the metaTreeStates function
    */
  def requestDataToSortedTags(request: GenMap[LocalODPair, GenSeq[List[SORoutingPathSegment]]]): Map[Tag.PersonID, Tag.PersonAlternatePaths] =
    Tag.repackage(request).toList.sortBy(_._2.size).toMap


}
