package cse.bdlab.fitzgero.sorouting.util.livy

import java.io.File
import java.net.URI

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}
import scala.concurrent.ExecutionContext.Implicits.global

import org.apache.livy.LivyClientBuilder
import org.apache.livy.scalaapi._

trait SORoutingSimpleBlockingLivyClient {

  /**
    * Creates a new Spark session managed by Livy
    * @param jarLocation the file system path to the jar that packages the job class we are about to call
    * @param livyURL the URL for the Livy server, which by default is localhost port 8998
    * @return the client object for the ongoing Spark session
    */
  def createAndActivateLivyClient(jarLocation: String, livyURL: String = "http://localhost:8998"): Option[LivyScalaClient] = {
    println()
    Try[LivyScalaClient]{
      val client = new LivyClientBuilder(false).setURI(new URI(livyURL)).build.asScalaClient
      val future = for {
        _ <- client.uploadJar(new File("lib/livy-scala-api_2.11-0.5.0-incubating.jar"))
        f <- client.uploadJar(new File(jarLocation))
      } yield f
      Await.result(future, 1 minute)
      client
    } match {
      case Success(result) =>
        Some(result)
      case Failure(e) =>
        println(s"Failed to provision SORoutingSimpleBlockingLivyClient: ${e.getMessage}")
        println(e.printStackTrace())
        None
    }
  }

  /**
    * tear-down a currently running Spark service managed via a LivyClient
    * @param client the client session to deactivate
    */
  def deactivateLivyClient(client: LivyScalaClient): Unit = {
    client.stop(true)
  }
}
