package cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.pedrosorei

import cse.bdlab.fitzgero.mcts.core.terminationcriterion.TimeTermination
import cse.bdlab.fitzgero.sorouting.TestTemplate
import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.RoutingSelectionPedrosoReiMCTSSolver

class RoutingSelectionPedrosoReiMCTSSolverTests extends TestTemplate {
  // designed to test this SO-Routing-Models package. maybe needs a looking into for correctness, as
  // many changes happened after this was written.
  "test sub project" ignore {
    "called with a test case" should {
      "run MCTS" in new TestAssets.CombinationSet {
        val solver = RoutingSelectionPedrosoReiMCTSSolver(graph, kspResult, 0, TimeTermination(5000L), Seq())
        val tree = solver.run()
        println(tree.visits)
        println(tree.bestSimulation)
        println(solver.bestSolution)
      }
    }
  }
}
