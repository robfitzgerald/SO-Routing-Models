package cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.monolithic

import cse.bdlab.fitzgero.sorouting.TestTemplate
import cse.bdlab.fitzgero.sorouting.model.path.SORoutingPathSegment
import cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.HackSerializer

class MonolithicLivyOpsTests extends TestTemplate {
  "diveDepth" when {
    "no minThreshold" when {
      "called with an empty request" should {
        "produce 0 representing no leaves of the meta tree" in {
          MonolithicLivyOps.diveDepth(Map(), 0, 1) should equal (0)
        }
      }
      "called with a 1-driver request, 3 alts, dive depth of 1" should {
        "produce 0" in new TestAssets.SingleDriver {
          MonolithicLivyOps.diveDepth(MonolithicLivyOps.requestDataToSortedTags(request), 0, threshold) should equal (diveDepth)
        }
      }
      "called with a threshold that exceeds the possible tree depth" should {
        "stop at the maximum tree depth" in new TestAssets.FourDriver {
          MonolithicLivyOps.diveDepth(MonolithicLivyOps.requestDataToSortedTags(request), 0, 9999999) should equal (4)
        }
      }
      "called with a 4-driver request, each driver w/ 3 alts" should {
        "produce the correct depth values for different boundary thresholds" in new TestAssets.FourDriver {
          // depth of 1 from a threshold of 3^0 + 1
          MonolithicLivyOps.diveDepth(MonolithicLivyOps.requestDataToSortedTags(request), 0, 2) should equal (0)
          // depth of 2 from a threshold of 3^1 + 1
          MonolithicLivyOps.diveDepth(MonolithicLivyOps.requestDataToSortedTags(request), 0, 4) should equal (1)
          // depth of 3 from a threshold of 3^2 + 1
          MonolithicLivyOps.diveDepth(MonolithicLivyOps.requestDataToSortedTags(request), 0, 10) should equal (2)
          // depth of 4 from a threshold of 3^3 + 1
          MonolithicLivyOps.diveDepth(MonolithicLivyOps.requestDataToSortedTags(request), 0, 28) should equal (3)
          // depth of 4 from a threshold of 3^4 + 1
          MonolithicLivyOps.diveDepth(MonolithicLivyOps.requestDataToSortedTags(request), 0, 82) should equal (4)
          // all higher values
          MonolithicLivyOps.diveDepth(MonolithicLivyOps.requestDataToSortedTags(request), 0, Int.MaxValue) should equal (4)
        }
      }
    }
    "with minThreshold" when {
      "called with a result that is below the min threshold" should {
        "return 0" in new TestAssets.FourDriver {
          MonolithicLivyOps.diveDepth(MonolithicLivyOps.requestDataToSortedTags(request), 100, 200) should equal (0)
        }
      }
    }
  }
  "metaTreeStates" when {
    "called on an empty request with a dive depth of 0" should {
      "return an empty Seq representing no leaves of the meta tree" in {
        MonolithicLivyOps.metaTreeStates(Map(), 0) should equal (List(List()))
      }
    }
    "called on a request with one driver and 3 alts, with a dive depth of 1" should {
      "produce a Seq with 3 inner Seqs, each with 1 of the driver's alts" in new TestAssets.SingleDriver {
        // the alternates should be 0, 1, and 2
        MonolithicLivyOps.metaTreeStates(MonolithicLivyOps.requestDataToSortedTags(request), 1).zip(0 to 2).foreach {
          tuple =>
            tuple._1.size should equal (1)
            tuple._1.head.personId should equal ("a")
            tuple._1.head.alternate should equal (tuple._2)
        }
      }
    }
    "called with a 4-driver request, each driver w/ 3 alts" should {
      "produce AlternateSets of the correct size, representing the leaves of the meta tree at that depth" in new TestAssets.FourDriver {
        MonolithicLivyOps.metaTreeStates(MonolithicLivyOps.requestDataToSortedTags(request), 0).distinct.size should equal (1)
        MonolithicLivyOps.metaTreeStates(MonolithicLivyOps.requestDataToSortedTags(request), 1).distinct.size should equal (3)
        MonolithicLivyOps.metaTreeStates(MonolithicLivyOps.requestDataToSortedTags(request), 2).distinct.size should equal (9)
        MonolithicLivyOps.metaTreeStates(MonolithicLivyOps.requestDataToSortedTags(request), 3).distinct.size should equal (27)
        MonolithicLivyOps.metaTreeStates(MonolithicLivyOps.requestDataToSortedTags(request), 4).distinct.size should equal (81)
      }
    }
    "called with a 3-driver request, but two have no alts" should {
      "produce AlternateSets of the correct size, representing the leaves of the meta tree at that depth" in new TestAssets.ThreeWithAFewThatHaveNoAlts {
        MonolithicLivyOps.metaTreeStates(MonolithicLivyOps.requestDataToSortedTags(request), 1).distinct.size should equal (MonolithicLivyOps.metaTreeStates(MonolithicLivyOps.requestDataToSortedTags(reverseOrder), 1).distinct.size)
        MonolithicLivyOps.metaTreeStates(MonolithicLivyOps.requestDataToSortedTags(request), 2).distinct.size should equal (MonolithicLivyOps.metaTreeStates(MonolithicLivyOps.requestDataToSortedTags(reverseOrder), 2).distinct.size)
        MonolithicLivyOps.metaTreeStates(MonolithicLivyOps.requestDataToSortedTags(request), 3).distinct.size should equal (MonolithicLivyOps.metaTreeStates(MonolithicLivyOps.requestDataToSortedTags(reverseOrder), 3).distinct.size)
      }
    }
  }
  "flowSnapshot encode/decode" when {
    "passed a graph through both operations" should {
      "be idempotent" in new TestAssets.graphSquare9PeopleThreeWays {
        val flows = HackSerializer.stringDecodeFlowSnapshot(HackSerializer.stringEncodeFlowSnapshot(graph))
        flows.foreach {
          tuple =>
            graph.edgeById(tuple._1) match {
              case None => fail() // edge should exist
              case Some(e) => e.attribute.flow match {
                case None =>
                  tuple._2 should equal (0)
                case Some(f) =>
                  tuple._2 should equal (f.toInt)
              }
            }
        }
        flows.size should equal (graph.edges.size)
      }
    }
  }
  "request encode/decode" when {
    "passed a request through both operations" should {
      "be idempotent" in new TestAssets.BiggerMap {
        val result = HackSerializer.stringDecodeRequest(HackSerializer.stringEncodeRequest(kspResult))

        result.foreach {
          person =>
            kspResult.isDefinedAt(person._1) should be (true)
            person._2.foreach {
              alt =>
                val altsToTest: Seq[Seq[SORoutingPathSegment]] = kspResult(person._1)
                val hasMatch = altsToTest.exists {
                  xs: Seq[SORoutingPathSegment] => xs == alt
                }
                hasMatch should equal (true)
            }
        }

        result.size should equal (kspResult.size)
      }
    }
  }
}
