package cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.iterative

import cse.bdlab.fitzgero.mcts.algorithm.samplingpolicy.banditfunction.UCT_PedrosoRei.Minimize
import cse.bdlab.fitzgero.mcts.algorithm.samplingpolicy.scalar.UCTScalarPedrosoReiReward.Coefficients
import cse.bdlab.fitzgero.mcts.tree.MCTreePedrosoReiReward
import cse.bdlab.fitzgero.sorouting.algorithm.local.selection.mcts.Tag

object TestAssets {

  trait NumChildrenHelperSimple {
    val counts = Map(0->3,1->2,2->4)
    val state: Seq[Tag] = Seq(Tag("1",1),Tag("2",2)) // child counts should equal 4
  }

  trait NumChildrenHelperComplete {
    val counts = Map(0->3,1->2,2->4)
    val state: Seq[Tag] = Seq(Tag("1",1),Tag("2",2),Tag("3",2)) // child counts should equal 4
  }

  trait ChildrenToKeepBaseCase {
    val coefficients: Coefficients = Coefficients(1.414D, 0, 100)
    val tree: MCTreePedrosoReiReward[Tag.AlternatesSet, Tag] =
      MCTreePedrosoReiReward(
        state = Seq(),
        action = None,
        objective = Minimize()
      )
    tree.visits = 2
    tree.reward(coefficients)

    val child1: MCTreePedrosoReiReward[Tag.AlternatesSet, Tag] =
      MCTreePedrosoReiReward(
        state = Seq(Tag("a",1)),
        action = Some(Tag("a",1)),
        objective = Minimize()
      )
    child1.bestSimulation = 0
    child1.averageSum = 25
    child1.visits = 1
    child1.reward(coefficients)

    val child2: MCTreePedrosoReiReward[Tag.AlternatesSet, Tag] =
      MCTreePedrosoReiReward(
        state = Seq(Tag("a",2)),
        action = Some(Tag("a",2)),
        objective = Minimize()
      )
    child2.bestSimulation = 50
    child2.averageSum = 75
    child2.visits = 1
    child2.reward(coefficients)

    tree.addChild(child1)
    tree.addChild(child2)

  }
}
