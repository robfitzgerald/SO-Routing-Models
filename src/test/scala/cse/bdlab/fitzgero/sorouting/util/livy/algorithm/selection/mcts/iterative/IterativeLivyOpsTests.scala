package cse.bdlab.fitzgero.sorouting.util.livy.algorithm.selection.mcts.iterative

import cse.bdlab.fitzgero.sorouting.TestTemplate

class IterativeLivyOpsTests extends TestTemplate {
  "IterativeLivyOps" when {
    "numChildrenHelper" when {
      "given a simple input" should {
        "provide the correct response" in new TestAssets.NumChildrenHelperSimple {
          IterativeLivyOps.numChildrenHelper(counts)(state) should equal (4)
        }
      }
      "called on a complete solution" should {
        "provide 0" in new TestAssets.NumChildrenHelperComplete {
          IterativeLivyOps.numChildrenHelper(counts)(state) should equal (0)
        }
      }
    }
    "childrenToKeep" when {
      "called with a tree with two children, only one above the threshold" should {
        "filter the lower-valued child" in new TestAssets.ChildrenToKeepBaseCase {
          val childrenToKeep = IterativeLivyOps.childrenToKeep(tree, coefficients, 1.0D)
          childrenToKeep.size should equal (1)
          childrenToKeep.head.state should equal (child1.state)
        }
      }
    }
    "frontierDepth" when {
      "called on an empty list" should {
        "suggest a frontier depth of 0" in {
          val emptyAlts = List.empty[Int]
          val result = IterativeLivyOps.frontierDepth(emptyAlts, 4, 4, 4)
          result should equal (0)
        }
      }
      "called on a list that represents a set of alternates which is within the user's payload budget" should {
        "suggest a depth equal to the full size of the list" in {
          val withinBudget = List(2,3,2) // total set would be 2 * 3 * 2 = 12 alts
          val result = IterativeLivyOps.frontierDepth(withinBudget, 4, 4, 4) // can handle a workload of 4 * 4 * 4 = 64 payloads total
          result should equal (3)
        }
      }
      "called on a list that represents a set of alternates which exceeds the user's payload budget" should {
        "select a depth which would appropriately truncate the search" in {
          val withinBudget = List(3,3,3,3,3) // total set would be 3^5 = 243 alts
          val result = IterativeLivyOps.frontierDepth(withinBudget, 4, 4, 4) // can only handle 3^3 of the payloads payloads total
          result should equal (3)
        }
      }
    }
    "fillPartitionBudget" when {
      "called with room to duplicate" should {
        "return exactly enough payloads to fit our budget" in {
          // written programmatically to support property testing refactor
          val combinations = List(1,2,3,4,5)
          val executors = 4
          val partitions = 4
          val filled = IterativeLivyOps.fillPartitionBudget(combinations, executors, partitions)
          filled.size should equal (executors * partitions)
          filled.
            sliding(combinations.size, combinations.size).
            foreach { group =>
              for {
                i <- group.indices
              } combinations(i) should equal (group(i))
            }
        }
      }
    }
  }
}
