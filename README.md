# System-Optimal Routing (SO Routing) Models

Using Apache Spark with Livy allows us to send work to Spark from our test bed. In order to acheive this, we need to ship the code we will call from within Spark as a jar.

This project contains the code and dependencies we will call from Spark.

More on Livy [here](https://livy.incubator.apache.org/).