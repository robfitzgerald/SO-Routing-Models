name := "SO-Routing-Models"

version := "1.4.7"

scalaVersion := "2.11.11"

// scala xml
libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.0.2"

// ~~~ ScalaTest
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.4"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.4" % "test"
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.13.4" % "test"

// ~~~ Livy
// REST/Programmatic Spark Job Submission
libraryDependencies += "org.apache.livy" % "livy-client-http" % "0.5.0-incubating"
//libraryDependencies += "org.apache.livy" %% "livy-core" % "0.5.0-incubating"
libraryDependencies += "org.apache.livy" %% "livy-scala-api" % "0.5.0-incubating"

// ~~~ Spark
libraryDependencies += "org.apache.spark" %% "spark-core" % "2.2.0" % "provided" /*exclude("ch.qos.logback", "*")*/
libraryDependencies += "org.apache.spark" %% "spark-graphx" % "2.2.0" % "provided" /*exclude("ch.qos.logback", "*")*/


test in assembly := {}

assemblyMergeStrategy in assembly := {
  case "META-INF/services/org.apache.livy.LivyClientFactory" => MergeStrategy.last
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
